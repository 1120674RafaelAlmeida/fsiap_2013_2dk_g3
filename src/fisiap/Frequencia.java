package fisiap;

import java.math.BigDecimal;

public class Frequencia {
    double ComprimentoOnda;
    double frequencia;
    public Frequencia(){
        setFrequencia(1600);
        setComprimentoOnda(18.75);
    }
    
    public Frequencia(double frequencia, double ComprimentoOnda){
        setFrequencia(calcularComComprimentoOnda(ComprimentoOnda));
        setComprimentoOnda(calcularComFrequencia(frequencia));
    }


    public double calcularComFrequencia(double frequencia){
        double returnState, returnState2;
        returnState2=300000000/(frequencia*10000);
        returnState=round(returnState2, 2);
        return returnState;
    }
    
    
    public double calcularComComprimentoOnda(double frequencia){
        double returnState, returnState2; 
        returnState2=300000000/(frequencia/10000);
        returnState2=returnState2/100000000;
        returnState=round(returnState2, 0);
        return returnState;
    }
            
    @Override
    public String toString() {
        return "frequencia{" + "ComprimentoOnda=" + ComprimentoOnda + ", frequencia=" + frequencia + '}';
    }

    public double getComprimentoOnda() {
        return ComprimentoOnda;
    }

    public void setComprimentoOnda(double ComprimentoOnda) {
        this.ComprimentoOnda = ComprimentoOnda;
        this.frequencia = calcularComComprimentoOnda(ComprimentoOnda)*1000000;
    }

    public double getFrequencia() {
        return frequencia;
    }

    public void setFrequencia(double frequencia) {       
        this.ComprimentoOnda = calcularComFrequencia(frequencia);
        this.frequencia = frequencia*1000000;
    }
    
    
    
    public static double round(double value, int places) {
    if (places < 0) throw new IllegalArgumentException();

    BigDecimal bd = new BigDecimal(value);
    bd = bd.setScale(places, BigDecimal.ROUND_HALF_UP);
    return bd.doubleValue();
}
}
