package fisiap;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
//import javax.swing.*;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class Index extends JFrame {
    JMenuItem item;
    JMenu mnuAjuda;
    int FPS_MIN = 700;
    int FPS_MAX = 2500;
    int FPS_INIT = 1600;
    JSlider sliderFrequency, sliderOnda, sliderTemperature;
    JTextField mostrarFrequencia, mostrarComprimentoOnda, mostrarTemperatura ;
    JLabel Mhz, cm, temp, Nota;
    Frequencia frequency= new Frequencia();
    FrequenciaAngular angularFrequency=new FrequenciaAngular();
    Temperatura temperature=new Temperatura();
    Energia energy=new Energia();
    
    Potencia power=new Potencia();
    NumberFormat formatter = new DecimalFormat();
    
    public Index() {
        super("Radiação electromagnética");
        JPanel container = new JPanel();
        container.setLayout(new BoxLayout(container, BoxLayout.Y_AXIS));
        JPanel mainPanel = new JPanel();
        add(container);
        mainPanel.setMaximumSize(new Dimension(400, 50));
        container.add(mainPanel);
             //   System.out.println(angularFrequency.getFrequenciaAngular());
        
//        JMenuBar mb = new JMenuBar();
//        setJMenuBar(mb);
//        mnuAjuda = new JMenu("Ajuda");mb.add(mnuAjuda);
//        item = new JMenuItem("Sobre");mnuAjuda.add(item);
//        item.addActionListener(new ActionListener() {
//            public void actionPerformed(ActionEvent e) {
//                JOptionPane.showMessageDialog(null, "Projecto de FISIAP\n"
//                        + "Programado por:\n"
//                        + "Rafael Almeida\n"
//                        + "Rafael Almeida");
//            }
//        });
        sliderFrequency = new JSlider(JSlider.HORIZONTAL, FPS_MIN, FPS_MAX, FPS_INIT);
        sliderFrequency.setMajorTickSpacing(500);
        sliderFrequency.setMinorTickSpacing(100);
        sliderFrequency.setPaintTicks(true);
        sliderFrequency.setPaintLabels(true);
        mostrarFrequencia = new JTextField("1600");
        mostrarComprimentoOnda = new JTextField("18.75");
        mostrarComprimentoOnda.setEditable(false);
        mainPanel.add(sliderFrequency);
        
        Mhz= new JLabel("Mhz");       
        cm= new JLabel("cm");
        
        // SLIDER DE FREQUENCIA
        sliderFrequency.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                JSlider source = (JSlider) e.getSource();
                if (!source.getValueIsAdjusting()) {
                    mostrarFrequencia.setText(String.valueOf(sliderFrequency.getValue()));
                    frequency.setFrequencia(Double.parseDouble(String.valueOf(sliderFrequency.getValue())));
                    mostrarComprimentoOnda.setText(frequency.getComprimentoOnda() + "");
                    energy.calculaEnergia(frequency.getFrequencia());
                    angularFrequency.setFrequencia(frequency.getFrequencia());
                    double trim=frequency.getComprimentoOnda()*100;
                    int trim2=(int)trim;
                    sliderOnda.setValue(trim2);
                    //POWER BLOCK
                    power.setFrequenciaAngular(angularFrequency.getFrequenciaAngular());
                    power.setEnergia(energy.getEnergia());
                    power.calcular();
                    
                    
                    temperature.alteraTemperaturaPotencia(power.getPotencia());
                    
                    formatter = new DecimalFormat("0.##E0");
                    
                    mostrarTemperatura.setText(formatter.format(temperature.getTemperatura()));
                    String s =formatter.format(temperature.getTemperatura()); 
                    
                    if(s.contains("30")){
                        
                        String[] string=s.split("E");
                        
                        string[0]=string[0].replace(',', '.');
                        double d=Double.parseDouble(string[0]);
                        d=d*100+1000;
                        int i=(int)d;
                       // System.out.println(i);
                        sliderTemperature.setValue(i);
                                }
                    else{
                        String[] string=s.split("E");
                        
                        string[0]=string[0].replace(',', '.');
                        double d=Double.parseDouble(string[0]);
                        d=d*100;
                        int i=(int)d;
                       // System.out.println(i);
                        sliderTemperature.setValue(i);
                    }
                  //  System.out.println(power.getPotencia());
                   // System.out.println(temperature.getTemperatura());
                }

            }
        });
             
        // TEXTO DE FREQUENCIA
        mostrarFrequencia.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try
                {
                    sliderFrequency.setValue(Integer.parseInt(mostrarFrequencia.getText()));
                    frequency.setFrequencia(Double.parseDouble(mostrarFrequencia.getText()));
                    mostrarComprimentoOnda.setText(frequency.getComprimentoOnda() + "");
                    double trim=frequency.getComprimentoOnda()*100;
                    int trim2=(int)trim;
                    sliderOnda.setValue(trim2);
                    energy.calculaEnergia(Double.parseDouble(String.valueOf(sliderFrequency.getValue())));
                    angularFrequency.setFrequencia(Double.parseDouble(String.valueOf(sliderFrequency.getValue())));
                    //POWER BLOCK
                    power.setFrequenciaAngular(angularFrequency.getFrequencia());
                    power.setEnergia(energy.getEnergia());
                    power.calcular();
                }
                catch(Exception ex)
                {
                    mostrarFrequencia.setText("ERROR");
                    mostrarFrequencia.setToolTipText("Set Value in Range between 700 - 2500 ") ;
                }
            }
        });
        
        
        JPanel secondaryPanel = new JPanel();
        secondaryPanel.setMaximumSize(new Dimension(400, 50));
        container.add(secondaryPanel);
        sliderOnda = new JSlider(JSlider.HORIZONTAL, 1200, 4286, 1875);
        sliderOnda.setMajorTickSpacing(1000);
        sliderOnda.setMinorTickSpacing(200);
        sliderOnda.setPaintTicks(true);
        sliderOnda.setPaintLabels(false);
        secondaryPanel.add(sliderOnda);
        
        
        // SLIDER DE COMPRIMENTO DE ONDA
        sliderOnda.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                JSlider source = (JSlider) e.getSource();
                if (!source.getValueIsAdjusting()) {
                    BigDecimal numero = new BigDecimal(sliderOnda.getValue());
                    BigDecimal cem = new BigDecimal("100");
                    BigDecimal bg = numero.divide(cem, 2, RoundingMode.CEILING);
                    mostrarComprimentoOnda.setText(String.valueOf(bg));
                    frequency.setComprimentoOnda(Double.parseDouble(String.valueOf(bg)));
                    int a= (int)(frequency.getFrequencia()/1000000);
                    mostrarFrequencia.setText(a + "");
                    sliderFrequency.setValue(a);
                }

            }
        });
        
        JPanel tercearyPanel = new JPanel();
        tercearyPanel.setMaximumSize(new Dimension(400, 30));
        container.add(tercearyPanel);
        tercearyPanel.add(mostrarFrequencia);
        tercearyPanel.add(Mhz);
        tercearyPanel.add(mostrarComprimentoOnda);
        tercearyPanel.add(cm);
        
        
        JPanel quaternaryPanel = new JPanel();
        quaternaryPanel.setMaximumSize(new Dimension(400, 100));
        container.add(quaternaryPanel);
        
        temp= new JLabel("Cº");
        
        mostrarTemperatura = new JTextField(temperature.getTemperatura()+"",5);
        
        sliderTemperature = new JSlider(JSlider.HORIZONTAL, 167,1761,  1200);
        sliderTemperature.setMajorTickSpacing(500);  
        sliderTemperature.setMinorTickSpacing(100);
        sliderTemperature.setPaintTicks(true);
        sliderTemperature.setPaintLabels(false);
        
        
        // SLIDER DE TEMPERATURA
        sliderTemperature.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                JSlider source = (JSlider) e.getSource();
                if (!source.getValueIsAdjusting()) {
                    String stringValorTemperatura=String.valueOf(sliderTemperature.getValue());
                    double doubleValorTemperatura=Double.parseDouble(stringValorTemperatura);

                    if (doubleValorTemperatura>=1000){
                        doubleValorTemperatura=(doubleValorTemperatura-1000)/100;
                        doubleValorTemperatura=doubleValorTemperatura*Math.pow(10, -30);
                    }else{
                        doubleValorTemperatura=(doubleValorTemperatura)/100;
                        doubleValorTemperatura=doubleValorTemperatura*Math.pow(10, -31);
                    }
                  //  System.out.println(doubleValorTemperatura);
                    formatter = new DecimalFormat("0.##E0");
                    
                    mostrarTemperatura.setText(formatter.format(temperature.getTemperatura()));
                    stringValorTemperatura =formatter.format(doubleValorTemperatura); 
                    mostrarTemperatura.setText(stringValorTemperatura);
                    
                    temperature.setTemperatura(doubleValorTemperatura);
                    temperature.calculaPotenciaTemp(doubleValorTemperatura);
                    //POWER BLOCK
                    
                    power.setPotencia(temperature.getPotencia());
                    power.calcularEnergia();
                    
                    energy.calculaFrequencia(power.getEnergia());
                    System.out.println(energy.getFrequencia());
                }

            }
        });
        Nota= new JLabel("<html><font color='red' align=center>Não é possível usar o ouput temperatura <br> como input para achar a frequência.</font></html>");
        quaternaryPanel.add(sliderTemperature);
        quaternaryPanel.add(mostrarTemperatura);
        quaternaryPanel.add(temp);
        quaternaryPanel.add(Nota);

        setSize(400, 400);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
    }

    public static void main(String[] args) {
        new Index();
    }
}