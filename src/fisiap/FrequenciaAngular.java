package fisiap;

public class FrequenciaAngular {

    
    public FrequenciaAngular(){
        setFrequenciaAngular(2*Math.PI*1600000000);
        setFrequencia(1600000000);
    }
    
    public double getFrequenciaAngular() {
        return frequenciaAngular;
    }

    public void setFrequenciaAngular(double frequenciaAngular) {
        this.frequenciaAngular = frequenciaAngular;
    }

    public double getFrequencia() {
        return frequencia;
    }

    public void setFrequencia(double frequencia) {
        this.frequencia = frequencia;
        this.frequenciaAngular = (2*Math.PI*(frequencia));
    }
    double frequenciaAngular;
    double frequencia;
}
