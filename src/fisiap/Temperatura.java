package fisiap;

import java.math.BigDecimal;

public class Temperatura {

    private double temperatura;
    private double area;
    private double potencia;

    public Temperatura() {
        this.setTemperatura(2 * Math.pow(10, -30));
        this.setArea(0.05);
        this.setPotencia(8.85 * Math.pow(10, -50));
    }

    //Método para calcular a raíz quarta de um número
    public double Get4Root(double n, double low, double high) {
        double errorMargin = 0.001;
        double g4rt = (low + high) / 2;
        double diff = g4rt * g4rt * g4rt - n;
        if (diff > errorMargin) {
            return Get4Root(n, low, g4rt);
        }
        if (-diff > errorMargin) {
            return Get4Root(n, g4rt, high);
        }
        return g4rt;
    }

    public double G4rt(double n) {
        return Get4Root(n, 0, n);
    }

    //Método para calcular a temperatura, indicando a frequencia e a area de efeito
    public void calculaTemperatura(double pot, double ar) {

        //P = E *ró* T^(4) ==> Potência = Emissividade = 0.98  *ró* = 5.67*10^(-8)  Temperatura
        double temp = (potencia) / (0.98 * 5.67 * Math.pow(10, -8) * area);
        this.setTemperatura(G4rt(temp));

    }

    public void calculaPotenciaTemp(double temp) {

        double pot = Math.pow(temp, 4) * (0.98 * 5.67 * Math.pow(10, -8)) * area;
        this.setPotencia(pot);
    }

    public void alteraTemperaturaPotencia(double pot) {
        this.setPotencia(pot);

        double temp = (potencia) / (0.98 * 5.67 * Math.pow(10, -8) * area);
        this.setTemperatura(G4rt(temp));

    }

    public void alteraTemperaturaArea(double ar) {
        this.setArea(ar);

        double temp = (potencia) / (0.98 * 5.67 * Math.pow(10, -8) * area);
        this.setTemperatura(G4rt(temp));

    }

    /**
     * @return the temperatura
     */
    public double getTemperatura() {
        return temperatura;
    }

    /**
     * @param temperatura the temperatura to set
     */
    public void setTemperatura(double temperatura) {
        this.temperatura = temperatura;
    }

    /**
     * @return the area
     */
    public double getArea() {
        return area;
    }

    /**
     * @param area the area to set
     */
    public void setArea(double area) {
        this.area = area;
    }

    /**
     * @return the potencia
     */
    public double getPotencia() {
        return potencia;
    }

    /**
     * @param potencia the potencia to set
     */
    public void setPotencia(double potencia) {
        this.potencia = potencia;
    }
}
