package fisiap;

public class Potencia {
    double potencia;
    double energia;
    double frequenciaAngular;
    double permissividade;

    public Potencia(){
        setPotencia(8.85 * Math.pow(10,(-44)));
        setEnergia(Math.pow(10,(-24)));
        setFrequenciaAngular(Math.pow(10,(10)));
        setPermissividade(0.98);
    }
    
    public double getEnergia() {
        return energia;
    }

    public void setEnergia(double energia) {
        this.energia = energia;
    }

    public double getFrequenciaAngular() {
        return frequenciaAngular;
    }

    public void setFrequenciaAngular(double frequenciaAngular) {
        this.frequenciaAngular = frequenciaAngular;
    }

    public double getPermissividade() {
        return permissividade;
    }

    public void setPermissividade(double permissividade) {
        this.permissividade = permissividade;
    }
    public void calcularEnergia(){
        this.energia=Math.sqrt(this.potencia/(this.frequenciaAngular*this.permissividade));
    }
    
    public void calcular(){
        this.potencia=this.frequenciaAngular*this.permissividade*Math.pow(this.energia,2);
    }
    
    
    public double getPotencia() {
        return potencia;
    }

    public void setPotencia(double potencia) {
        this.potencia = potencia;
    }
    
    
}
