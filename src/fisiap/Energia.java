package fisiap;

import java.math.BigDecimal;

public class Energia {
    //E = h * f
    
    private double energia;
    private double frequencia;
    
    public Energia(){
        setEnergia(1.6 * Math.pow(10,9)*6.63 * Math.pow(10,-34));
        setFrequencia(1600000000);
    }
    
    public void calculaEnergia(double freq){
        this.setFrequencia(freq);
        
        setEnergia(getFrequencia() * 6.63 * Math.pow(10,-34));
    }
public void calculaFrequencia(double energia){
        this.setEnergia(energia);
        
        setFrequencia(getEnergia() / (6.63 * Math.pow(10,-34)));
    }

    public double getEnergia() {
        return energia;
    }


    public void setEnergia(double energia) {
        this.energia = energia;
    }


    public double getFrequencia() {
        return frequencia;
    }


    public void setFrequencia(double frequencia) {
        this.frequencia = frequencia;
    }
}
